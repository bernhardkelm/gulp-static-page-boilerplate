# Purpose of this project

This Boilerplate is made to be a small and easy way to create static Websites while enjoying the benefits of a powerful toolchain like CSS Pre-Processing, Babel, hot reload and more.
It includes:
- Babel (Default settings are last 4 Browser versions, IE >= 10, > 1%)
- Sourcemap generation
- SCSS
- Minification
- CSS Prefixing
- CSS Optimisation
- Image optimisation
- Hot reload in your dev enviroment
- ESLint (using the [Airbnb Styleguide](https://github.com/airbnb/javascript))

# Requirements
Since Node.js is needed to run the development server it is required for the project.
For more information check the [installation guide](https://nodejs.org/en/download/).

# Installation

run `npm install` in the working directory

# Usage
Your working directory is `/src` - here you can write SCSS, JS and HTML. Images used should be placed in the `assets` directory.
All files are processed and piped into the `/dist` directory which then can be served. For the purpose of development a small Node.js Server is included which takes the index.html file as an entrypoint and serves all files for local development.
By default the Server run's on localhost:3000 and the Hot-reload version on localhost:3001. This can be adjusted in the `config.js`.

# Commands
- `npm run start:dev` - Starts the development server and watches all files for hot-reloading.
- `npm run build` - Run's the entire build toolchain and outputs all files in the `/dist` directory for deployment
- `npm run build:css` - Only builds the CSS
- `npm run build:js` - Only builds the JavaScript
- `npm run build:html` - Only builds the HTML
- `npm run lint` - Runs the linter
- `npm start` - Runs the Node.js server standalone - you will need to build before this

# Notes
`no-restricted-syntax` for eslint has been disabled to allow the use of `for...of` loops. This is a controversial topic in the eslint community but `for...of` loops offer distinct advantages over functions like `forEach` - for example async iterations, iterating over sets, usage of break etc.
For a read on the pros/cons of different iterators go [here](https://medium.com/@nataliecardot/foreach-vs-for-of-vs-for-in-loops-472146fc1a1f). There is also an active discussion on Airbnb's [issue page](https://github.com/airbnb/javascript/issues/1271).

/* === Utility functions === */

// eslint-disable-next-line no-unused-vars
const Util = {
  /**
   * Make an XMLHTTP Request
   * @param {String} method - The HTTP Method to be used
   * @param {String} url - The request URL
   * @param {JSON} body - JSON to be sent (optional)
   */
  request: (method, url, body) => new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();

    req.onreadystatechange = () => {
      if (req.readyState !== 4) return;

      if (req.status >= 200 && req.status < 300) {
        resolve({
          status: req.status,
          statusText: req.statusText,
          response: req.response,
          responseText: req.responseText,
        });
      } else {
        reject(req);
      }
    };

    req.open(method, url);
    if (body) req.setRequestHeader('Content-Type', 'application/json');
    const payload = body ? JSON.stringify(body) : undefined;
    req.send(payload);
  }),

  /**
   * Select one or more DOM Elements by a 'data-js-*' attribute
   * @param {String} str - The element selector
   * @param {Boolean} multiple - Serch for one or multiple elements
   * @return {Node} elm - One or more found DOM Nodes, returns undefined if no element was present
   */
  select: (str, multiple = false) => {
    const selector = `[data-js-${str}]`;
    const elm = multiple ? document.querySelectorAll(selector) : document.querySelector(selector);
    return elm || undefined;
  },

  /**
   * Clearing the contents of a DOM element
   * @param {Node} elm - The node to be cleared
   */
  clear: (elm) => {
    while (elm.firstChild) { elm.removeChild(elm.firstChild); }
  },

  /**
   * Remove a DOM Element
   * @param {Node} elm - The DOM Node to be removed
   */
  remove: (elm) => {
    if (elm) elm.parentNode.removeChild(elm);
  },

  /**
   * Interting a DOM Node after another
   * @param {Node} newNode - The new DOM Node to be inserted
   * @param {Node} referenceNode - The reference node to insert after
   */
  insertAfter: (newNode, referenceNode) => {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  },

  /**
   * Convert's a file into a Base64 encoded String
   * @param  {File} str - The element selector
   * @return {String} reader.result - The Base64 encoded string
   */
  getBase64: (file) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (err) => reject(err);
  }),
};

/* === Query interaction functions === */

// eslint-disable-next-line no-unused-vars
const Query = {
  /**
   * Get the value of a querystring
   * @param {String} field - The field to get the value of
   * @param {String} url - The URL to get the value from (optional)
   * @return {String} - The field value
   */
  getQueryParameter: (field, url) => {
    const pageUrl = url || window.location.href;
    const reg = new RegExp(`[?&]${field}=([^&#]*)`, 'i');
    const string = reg.exec(pageUrl);
    return string ? string[1] : null;
  },

  /**
   * Remove a query parameter from an URL
   * @param {String} field - Field to remove from the URL
   * @param {String} url - The URL to get the value from (optional)
   * @return {String} - The new URL
   */
  removeQueryParameter: (field, url) => {
    let pageUrl = url || window.location.href;

    if (!this.getQueryParameter(field, url)) {
      return url;
    }

    pageUrl = `${pageUrl.split('?')[0]}?`;

    const urlVars = decodeURIComponent(window.location.search.substring(1)).split('&');

    for (const urlVar of urlVars) {
      const parameterName = urlVar.split('=');
      if (parameterName[0] !== field) {
        pageUrl = `${pageUrl}${parameterName[0]}=${parameterName[1]}&`;
      }
    }
    return url.substring(0, url.length - 1);
  },

  /**
   * Add a key/value to an URL
   * @param {String} key - The field to add
   * @param {String} val - The value for the field
   * @param {String} url - The URL to add the value to (optional)
   * @return {String} - The new URL
   */
  addQueryParameter: (key, val, url) => {
    let pageUrl = url || window.location.href;

    if (this.getQueryParameter(key, pageUrl)) {
      return this.replaceQueryParameter(key, val, pageUrl);
    }

    if (this.checkIfQueryExists(pageUrl)) {
      pageUrl += `&${key}=${val}`;
    } else {
      pageUrl += `?${key}=${val}`;
    }

    return pageUrl;
  },

  /**
   * Replace a query parameter with a new value
   * @param {String} key - The field to replace the value of
   * @param {String} val - The value to replace
   * @param {String} url - The URL to add the value to (optional)
   * @return {String} - The new URL
   */
  replaceQueryParameter: (key, val, url) => {
    const pageUrl = url || window.location.href;

    if (this.getQueryParameter(key, pageUrl) === val) {
      return pageUrl;
    }

    if (this.getQueryParameter(key, pageUrl)) {
      const re = new RegExp(`([?&])${key}=.*?(&|$)`, 'i');
      const separator = pageUrl.indexOf('?') !== -1 ? '&' : '?';
      if (pageUrl.match(re)) {
        return pageUrl.replace(re, `$1${key}=${val}$2`);
      }
      return `${pageUrl}${separator}${key}=${val}`;
    }

    return this.addQueryParameter(key, val, pageUrl);
  },

  /**
   * Check if the current URL has a query
   * @param {String} url - The URL you want to check (optional)
   * @return {Boolean} - If a query exists or not
   */
  checkIfQueryExists: (url) => {
    const pageUrl = url || window.location.href;
    if (pageUrl.indexOf('?') !== -1) return true;
    return false;
  },
};

const Page = {
  init: () => {
    // eslint-disable-next-line no-console
    console.log('JS is working fine');
  },
};

/* === On DOM load === */

/**
 * Init function, executed on document load
 * Move all functions which need to be executed on pageload here
 */
const init = () => {
  Page.init();
};

if (document.readyState === 'complete') {
  init();
} else {
  document.addEventListener('DOMContentLoaded', () => {
    init();
  });
}

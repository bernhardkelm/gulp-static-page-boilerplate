const config = {
  server: {
    protocol: 'http',
    host: 'localhost',
    port: 3000,
    workers: 2,
  },
};

module.exports = config;

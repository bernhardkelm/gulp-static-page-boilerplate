/* eslint-disable no-console */
/* === Extensions import === */

const http = require('http');
const express = require('express');
const cluster = require('cluster');
const fs = require('fs');
const os = require('os');
const config = require('./config.js');

const app = express();

/* === express configuration === */

app.use(express.static(`${__dirname}/dist/public`));


/* === Public Routes === */

// GET - Rendering the frontpage
app.get('/', (_req, res) => {
  res.sendFile(`${__dirname}/dist/index.html`);
});

app.get('*', (req, res) => {
  const potentialFilePath = `${__dirname}/dist/pages/${req.path}.html`;
  if (fs.existsSync(potentialFilePath)) {
    res.sendFile(potentialFilePath);
  } else {
    res.status(404).send();
  }
});


/* === Server startup === */

// Uncaught error handling
let workers = process.env.WORKERS || os.cpus().length;
if (workers > 4) workers = config.server.workers || 2;

if (cluster.isMaster) {
  console.log('start cluster with %s workers', workers);
  let index = 0;
  do {
    const worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);
    index += 1;
  } while (index < workers.length);

  cluster.on('exit', (worker) => {
    console.log('worker %s died. restart...', worker.process.pid);
    cluster.fork();
  });
} else {
  http.createServer(app).listen(config.server.port);
}

process.on('uncaughtException', (err) => {
  console.error(`${(new Date()).toUTCString()} uncaughtException: ${err.message}`);
  console.error(err.stack);
  process.exit(1);
});

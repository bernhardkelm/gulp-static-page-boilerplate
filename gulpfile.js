// Load plugins
const nodemon = require('gulp-nodemon');
const browsersync = require('browser-sync').create();
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const ejs = require('gulp-ejs');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('autoprefixer');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');
const minify = require('gulp-minify');
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');
const config = require('./config.js');

// Starting the webserver
function server(done) {
  let started = false;
  return nodemon({
    script: 'server.js',
    watch: ['server.js'],
  }).on('start', () => {
    if (!started) {
      started = true;
    }
    browsersync.stream();
    done();
  });
}

// BrowserSync
function browserSync(done) {
  browsersync.init({
    proxy: `${config.server.protocol}://${config.server.host}:${config.server.port}`,
    open: false,
    cors: true,
    port: config.server.port + 1,
  });
  done();
}

// Transpile, concatenate and minify CSS
function css() {
  return gulp
    .src('src/scss/**/*.scss')
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'expanded',
      errLogToConsole: true,
      sourceComments: 'map',
    }))
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(postcss([autoprefixer()]))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/public/css/'))
    .pipe(browsersync.stream());
}

// Transpile, concatenate and minify scripts
function js() {
  return gulp
    .src('src/js/**/*.js')
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(concat('script.js'))
    .pipe(minify({
      ext: {
        min: '.min.js',
      },
      noSource: true,
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/public/js/'))
    .pipe(browsersync.stream());
}

// Optimizing images
function images() {
  return gulp.src('src/assets/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/public/assets/images'));
}

// Moving fonts
function fonts() {
  return gulp.src('src/assets/fonts/**/*')
    .pipe(gulp.dest('dist/public/assets/fonts'));
}

// Moving the html
function moveIndex() {
  return gulp
    .src('src/ejs/index.ejs')
    .pipe(plumber())
    .pipe(ejs({}))
    .pipe(rename({ extname: '.html' }))
    .pipe(gulp.dest('dist'))
    .pipe(browsersync.stream());
}

function movePages() {
  return gulp
    .src('src/ejs/pages/**/*.ejs')
    .pipe(plumber())
    .pipe(ejs({}))
    .pipe(rename({ extname: '.html' }))
    .pipe(gulp.dest('dist/pages'))
    .pipe(browsersync.stream());
}

function cleanDist() {
  return gulp.src('dist', {
    read: false,
    allowEmpty: true,
  })
    .pipe(clean());
}

// Elementary tasks
gulp.task('css', css);
gulp.task('js', js);
gulp.task('ejs', gulp.parallel(moveIndex, movePages));
gulp.task('assets', gulp.parallel(images, fonts));
gulp.task('clean', cleanDist);

// Complex tasks
gulp.task('watchFiles', (done) => {
  gulp.watch('src/scss/**/*', css);
  gulp.watch('src/js/**/*', js);
  gulp.watch('src/assets/**/*', images, fonts);
  gulp.watch('src/ejs/**/*.ejs', gulp.series('ejs'));
  done();
});
gulp.task('build', gulp.series('clean', gulp.parallel('css', 'js', 'assets', 'ejs')));
gulp.task('watch', gulp.series('build', gulp.parallel('watchFiles', server, browserSync)));

// Default task
gulp.task('default', gulp.series('build'));
